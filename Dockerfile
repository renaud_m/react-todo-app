FROM node:12.16.2-alpine3.10
WORKDIR /app
COPY ./ ./
RUN yarn install
ENTRYPOINT [ "yarn", "start" ]
